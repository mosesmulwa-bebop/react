import { useState, useRef } from "react";
import Card from "../UI/Card";
import classes from "./AddUser.module.css";
import Button from "../UI/Button";
import ErrorModal from "../UI/ErrorModal";
import Wrapper from "../Helpers/Wrapper";

const AddUser = (props) => {
  const nameInputRef = useRef();
  const ageInputRef = useRef();

  const [errorState, setErrorState] = useState(false);
  const [errorTitle, setErrorTitle] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const addUserHandler = (event) => {
    event.preventDefault();

    const usernameValue = nameInputRef.current.value;
    const ageValue = ageInputRef.current.value;

    if (usernameValue.trim().length === 0) {
      setErrorTitle("Empty!!");
      setErrorMessage("Username is empty");
      //console.log("Empty Username");
      setErrorState(true);
      return;
    }

    if (ageValue.trim().length === 0) {
      setErrorTitle("Age!!");
      setErrorMessage("Age is empty");
      setErrorState(true);
      // console.log("Empty age");
      return;
    }

    const ageInt = parseInt(ageValue);

    if (ageInt < 0) {
      //console.log("Age is invalid");
      setErrorTitle("Invalid Age!!");
      setErrorMessage("Age entered is less than 0");
      setErrorState(true);

      return;
    }

    console.log(ageValue, usernameValue);
    const newUser = {
      id: Math.random(),
      username: usernameValue,
      age: ageValue,
    };
    props.onAddUser(newUser);
    //reset
    nameInputRef.current.value ="";
    ageInputRef.current.value="";
    // setAgeValue("");
    // setUsernameValue("");
  };

  const resetErrorHandler = () => {
    setErrorState(false);
  };

  return (
    <Wrapper>
      {errorState && (
        <ErrorModal
          title={errorTitle}
          message={errorMessage}
          resetError={resetErrorHandler}
        />
      )}

      <Card className={classes.input}>
        <form onSubmit={addUserHandler}>
          <label htmlFor="username">Username</label>
          <input
            id="username"
            type="text"
            
            ref={nameInputRef}
          ></input>
          <label htmlFor="age">Age (Years)</label>
          <input
            id="age"
            type="number"
            
            ref={ageInputRef}
          ></input>
          <Button type="submit" className={classes.button}>
            Add User
          </Button>
        </form>
      </Card>
    </Wrapper>
  );
};

export default AddUser;
