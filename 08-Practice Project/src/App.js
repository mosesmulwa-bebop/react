import React,{useState}from "react";
import AddUser from "./components/Users/AddUser";
import UsersList from "./components/Users/UsersList";

function App() {
  
 const myusers = [
    { id: 1, username: "stuff", age: 39 },
    { id: 3, username: "stuffy", age: 9 },
  ];
  const [myUserList, setMyUserList] = useState(myusers); 

  const addUserToList = user => {
    setMyUserList(prevUserList => {
      return [user,...prevUserList];
    });
  }

  return (
    <React.Fragment> 
      <AddUser onAddUser={addUserToList}/>
      <UsersList items={myUserList}/>
    </React.Fragment>
  );
}

export default App;
