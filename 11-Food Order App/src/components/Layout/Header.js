import classes from "./Header.module.css";
import React from "react";

import HeaderCartButton from "./HeaderCartButton";
import mealsImage from '../../assets/meals.jpg';

const Header = (props) => {
  return (
    <React.Fragment>
      <header className={classes.header}>
        <h1>Mulwa Meals</h1>
        < HeaderCartButton onButtonClick={props.onShowCart}/>
      </header>
      <div className={classes.main_image}>
        <img src={mealsImage}  alt="Table of food"/>
      </div>
    </React.Fragment>
  );
};

export default Header;
