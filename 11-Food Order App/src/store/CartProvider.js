import CartContext from "./cart-context";

import { useReducer } from "react";

const defaultCartState = {
  items: [],
  totalAmount: 0,
};
//{ id: "c1", name: "Sushi", amount: "2", price: 23.4 }
const cartReducer = (state, action) => {
  if (action.type === "ADD") {
    /*1. Check if item exists in state.items, if so update amount, else add
          2. Increment total amount
          Used concat because it gives you a brand new array.
          
          */
    let updatedTotalAmount =
      state.totalAmount + action.item.price * action.item.amount;
    let existingItemIndex = state.items.findIndex(
      (item) => item.id === action.item.id
    );

    let existingCartItem = state.items[existingItemIndex];

    let updatedItem;
    let updatedItems;

    if (existingCartItem) {
      updatedItem = {
        ...existingCartItem,
        amount: existingCartItem.amount + action.item.amount,
      };
      updatedItems = [...state.items];
      updatedItems[existingItemIndex] = updatedItem;
    } else {
      updatedItems = state.items.concat(action.item);
    }

    return { items: updatedItems, totalAmount: updatedTotalAmount };
  }
  if (action.type === "REMOVE") {
    /*1. Check if that id exists in state.items
         2. Decrement amount or remove it
         3. Decrement total amount*/
    const existingCartItemIndex = state.items.findIndex((item) => item.id === action.id);
    const existingItem = state.items[existingCartItemIndex];
    let updatedItems;
    if (existingItem.amount > 1) {
      const updatedCartItem = {
        ...existingItem,
        amount: existingItem.amount - 1,
      };
      updatedItems = [...state.items];
      updatedItems[existingCartItemIndex] = updatedCartItem;
    } else if (existingItem.amount === 1) {
      updatedItems = state.items.filter(item => item.id !== action.id);
    }
    const updatedTotalAmount = state.totalAmount - existingItem.price;
    return { items: updatedItems, totalAmount: updatedTotalAmount };
  }
};

const CartProvider = (props) => {
  const [cartState, dispatchCartAction] = useReducer(
    cartReducer,
    defaultCartState
  );

  const addItemToCartHandler = (item) => {
    dispatchCartAction({ type: "ADD", item: item });
  };
  const removeItemFromCartHandler = (id) => {
    dispatchCartAction({ type: "REMOVE", id: id });
  };

  const cartContext = {
    items: cartState.items,
    totalAmount: cartState.totalAmount,
    addItem: addItemToCartHandler,
    removeItem: removeItemFromCartHandler,
  };

  return (
    <CartContext.Provider value={cartContext}>
      {props.children}
    </CartContext.Provider>
  );
};
export default CartProvider;
